import marshmallow
from marshmallow import fields

from irmin_pyserver import data_validation
from irmin_pyserver import response


class NestedSchema(marshmallow.Schema):
    date_field = fields.DateTime()


class SampleSchema(marshmallow.Schema):
    string_field = fields.Str(required=True)
    int_field = fields.Int()
    nested_field = fields.Nested(NestedSchema)


def test_parse_errors() -> None:
    sample = {
        "int_field": "not a number",
        "nested_field": {"date_field": "some date"},
        # TODO: unknown field handling only supported in marshmallow 3
        "unknown_field": "some value",
    }
    data, errors = SampleSchema().load(sample)
    assert errors
    result = list(data_validation.parse_errors(errors, base_path="/prefix"))
    assert all(isinstance(error, response.Error) for error in result)
    assert all(error.code == "input_validation_error" for error in result)
    pointers = {error.source for error in result}
    expected_pointers = {
        "/prefix/string_field",
        "/prefix/int_field",
        "/prefix/nested_field/date_field",
    }
    assert pointers == expected_pointers
