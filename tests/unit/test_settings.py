import pytest

from irmin_pyserver import settings


@pytest.mark.parametrize(
    ("dict_a", "dict_b", "expected"),
    (
        pytest.param({"a": 1}, {"a": 2}, {"a": 2}, id="value_update"),
        pytest.param({"a": 1}, {"b": 2}, {"a": 1, "b": 2}, id="new_value"),
        pytest.param(
            {"a": {"b": 1}}, {"a": {"b": 2}}, {"a": {"b": 2}}, id="nested_update",
        ),
        pytest.param(
            {"a": {"b": 1}},
            {"a": {"c": 2}},
            {"a": {"b": 1, "c": 2}},
            id="nested_new_value",
        ),
        pytest.param({"a": 1}, {"a": {"b": 1}}, {"a": {"b": 1}}, id="into_dict"),
        pytest.param({"a": {"b": 1}}, {"a": 1}, {"a": 1}, id="into_value"),
    ),
)
def test_merge_dicts(dict_a: dict, dict_b: dict, expected: dict) -> None:
    settings.merge_dicts(dict_a, dict_b)
    assert dict_a == expected
