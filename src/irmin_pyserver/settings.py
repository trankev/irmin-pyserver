import copy
import logging
import os
import os.path
import typing

import toml

_logger = logging.getLogger(__name__)


DEFAULT_SETTINGS = {
    "SERVER": {"host": "0.0.0.0", "port": 8000, "debug": True},
    "LOGGING": {
        "version": 1,
        "formatters": {
            "brief": {
                "format": "[{asctime}.{msec:3.0f}[{levelname}]{message}",
                "datefmt": "%Y-%m-%dT%H:%M:%S",
                "style": "{",
            },
            "access_log": {
                "format": "[{asctime}.{msec:3.0f}][{host}] {request} {message} {status} {bytes}",
                "style": "{",
            },
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "level": "DEBUG",
                "formatter": "brief",
            },
            "access_log": {
                "class": "logging.StreamHandler",
                "level": "INFO",
                "formatter": "access_log",
            },
        },
        "loggers": {"sanic.access": {"handlers": ["access_log"], "propagate": 0}},
        "root": {"level": "DEBUG", "handlers": ["console"]},
    },
}


def merge_dicts(
    dict_into: typing.MutableMapping[str, typing.Any],
    to_merge: typing.Mapping[str, typing.Any],
) -> None:
    for key, value in to_merge.items():
        if isinstance(dict_into.get(key), dict) and isinstance(value, dict):
            merge_dicts(dict_into[key], value)
        else:
            dict_into[key] = value


def update_dict(
    to_update: dict,
    fields: typing.Sequence[str],
    value: typing.Any,
) -> None:
    for field in fields[:-1]:
        to_update = to_update.setdefault(field, {})
        if isinstance(to_update, list):
            try:
                field = int(field)
            except ValueError as exc:
                raise ValueError(
                    f"Cannot update dict key {fields}: list"
                    + f"got a non-integer field name {field}",
                ) from exc
        elif isinstance(to_update, dict):
            pass
        else:
            raise ValueError(
                f"Cannot update dict key {fields}: expected "
                + f"dict or list at {field}, got {to_update}",
            )
    to_update[fields[-1]] = value


class SettingsFactory:
    def __init__(self) -> None:
        self.config = copy.deepcopy(DEFAULT_SETTINGS)

    def get(self) -> dict:
        return self.config

    def read_toml(self, filenames: typing.Iterable[str]) -> None:
        for filename in filenames:
            if not os.path.isfile(filename):
                raise OSError(f"Cannot find settings file {filename}")
            _logger.info(f"Reading settings from {filename}")
            file_config = toml.load(filename)
            merge_dicts(self.config, file_config)

    def read_env(self, prefix: str) -> None:
        variable_prefix = f"{prefix.upper()}_"
        for key, value in os.environ.items():
            if not key.startswith(variable_prefix):
                continue
            _logger.info(f"Reading environment variable {key}")
            fields = key[len(variable_prefix):].split("_")
            update_dict(self.config, fields, value)

    def update(self, extra_config: dict) -> None:
        _logger.info(f"Reading extra settings {list(extra_config.keys())}")
        merge_dicts(self.config, extra_config)
