import argparse
import typing

import sanic

from irmin_pyserver import settings


def create_app(
        *,
        args: typing.Optional[typing.Sequence[str]] = None,
        extra_settings: typing.Optional[dict] = None,
) -> sanic.Sanic:
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", action="append", default=["./settings.toml"])
    parsed_args = parser.parse_args(args)
    settings_factory = settings.SettingsFactory()
    settings_factory.read_toml(parsed_args.config)
    if extra_settings:
        settings_factory.update(extra_settings)
    config = settings_factory.get()

    app = sanic.Sanic()
    app.config.update(config)
    return app


def run(app: sanic.Sanic) -> None:
    app.run(**app.config["SERVER"])
