import dataclasses
import typing


@dataclasses.dataclass(frozen=True)
class Error:
    code: str
    title: str
    source: typing.Optional[str] = None
    meta: typing.Mapping[str, typing.Any] = dataclasses.field(default_factory=dict)


@dataclasses.dataclass(frozen=True)
class Response:
    data: typing.Optional[typing.Any] = None
    errors: typing.Sequence[Error] = dataclasses.field(default_factory=lambda: ())
    metadata: typing.Mapping[str, typing.Any] = dataclasses.field(default_factory=dict)


class ErrorResponse(Exception):
    http_code = 400

    def __init__(
            self,
            errors: typing.Sequence[Error],
            *args: typing.Any,
            **kwargs: typing.Any,
    ) -> None:
        super().__init__(*args, **kwargs)
        self.response = Response(errors=errors)


class Unauthorized(ErrorResponse):
    http_code = 401


class Forbidden(ErrorResponse):
    http_code = 403


class NotFound(ErrorResponse):
    http_code = 404


class Conflict(ErrorResponse):
    http_code = 409


class UnprocessableEntity(ErrorResponse):
    http_code = 422


class GatewayError(ErrorResponse):
    http_code = 502


class GatewayTimeout(ErrorResponse):
    http_code = 504
