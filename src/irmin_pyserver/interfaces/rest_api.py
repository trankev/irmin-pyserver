from __future__ import annotations

import dataclasses
import logging
import os.path
import typing

import sanic
import ujson
import websockets

from irmin_pyserver import data_validation
from irmin_pyserver import requests
from irmin_pyserver import response as irmin_response
from irmin_pyserver.use_cases import base as use_cases


_logger = logging.getLogger(__name__)


def serialize_result(result: irmin_response.Response) -> dict:
    return {
        "data": result.data,
        "errors": [dataclasses.asdict(error) for error in result.errors],
        "metadata": result.metadata,
    }


PARAM_LOCATIONS = "view_args", "querystring", "json"


def http_view(
        use_case: typing.Type[use_cases.FunctionUseCase],
) -> typing.Callable[..., typing.Awaitable[sanic.response.HTTPResponse]]:
    async def handle_http_request(
            request: sanic.request.Request,
            **_: typing.Any,
    ) -> sanic.response.HTTPResponse:
        instance = use_case(request.app.resources)
        parser = data_validation.get_argparser()
        schema = instance.request_schema
        schema.context = {
            "resources": request.app.resources,
        }
        try:
            params = await parser.parse(schema, request, locations=PARAM_LOCATIONS)
            use_case_request = requests.Request(params=params)
            result = await instance.run(use_case_request)
        except irmin_response.ErrorResponse as exc:
            result = exc.response
            status = exc.http_code
        else:
            status = 400 if result.errors else 200
        response = serialize_result(result)
        return sanic.response.json(response, status=status)
    return handle_http_request


def websocket_view(
        use_case: typing.Type[use_cases.IteratorUseCase],
) -> typing.Callable[..., typing.Awaitable[None]]:
    async def handle_websocket_request(
        request: sanic.request.Request,
        ws: websockets.WebSocketCommonProtocol,
        **_: typing.Any,
    ) -> None:
        instance = use_case(request.app.resources)
        parser = data_validation.get_argparser()
        schema = instance.request_schema
        schema.context = {
            "resources": request.app.resources,
        }
        try:
            params = await parser.parse(schema, request, locations=PARAM_LOCATIONS)
            use_case_request = requests.Request(params=params)
            async for message in instance.run(use_case_request):
                response = serialize_result(message)
                await ws.send(ujson.dumps(response))
        except irmin_response.ErrorResponse as exc:
            response = serialize_result(exc.response)
            await ws.send(ujson.dumps(response))
    return handle_websocket_request


@dataclasses.dataclass(frozen=True)
class Route:
    use_case: typing.Type[use_cases.AbstractUseCase]
    path: str
    methods: typing.Sequence[str]


class Resource:
    def __init__(self, name: str) -> None:
        self.name = name
        self.list_path = f"/{self.name}"
        self.detail_path = os.path.join(self.list_path, "<uid>")
        self.routes: typing.List[Route] = []

    def add_custom_list_use_case(
            self,
            use_case: typing.Type[use_cases.AbstractUseCase],
            name: str,
            methods: typing.Sequence[str] = ("GET",),
    ) -> None:
        path = os.path.join(self.list_path, name)
        self.routes.append(Route(use_case=use_case, path=path, methods=methods))

    def add_custom_detail_use_case(
            self,
            use_case: typing.Type[use_cases.AbstractUseCase],
            name: str,
            methods: typing.Sequence[str] = ("GET",),
    ) -> None:
        path = os.path.join(self.detail_path, name)
        self.routes.append(Route(use_case=use_case, path=path, methods=methods))

    def add_list_use_case(self, use_case: typing.Type[use_cases.AbstractUseCase]) -> None:
        self.routes.append(Route(use_case=use_case, path=self.list_path, methods=("GET",)))

    def add_create_use_case(self, use_case: typing.Type[use_cases.AbstractUseCase]) -> None:
        self.routes.append(Route(use_case=use_case, path=self.list_path, methods=("POST",)))

    def add_retrieve_use_case(self, use_case: typing.Type[use_cases.AbstractUseCase]) -> None:
        self.routes.append(Route(use_case=use_case, path=self.detail_path, methods=("GET",)))

    def add_update_use_case(self, use_case: typing.Type[use_cases.AbstractUseCase]) -> None:
        self.routes.append(Route(use_case=use_case, path=self.detail_path, methods=("PUT",)))

    def add_patch_use_case(self, use_case: typing.Type[use_cases.AbstractUseCase]) -> None:
        self.routes.append(Route(use_case=use_case, path=self.detail_path, methods=("PATCH",)))

    def add_delete_use_case(self, use_case: typing.Type[use_cases.AbstractUseCase]) -> None:
        self.routes.append(Route(use_case=use_case, path=self.detail_path, methods=("DELETE",)))


class RestApi:
    def __init__(self) -> None:
        self.resources: typing.MutableMapping[str, Resource] = {}
        self.blueprint = sanic.Blueprint("rest_api")

    def add_resource(self, resource: Resource) -> None:
        for route in resource.routes:
            _logger.info(f"Registering {route.path}")
            if issubclass(route.use_case, use_cases.FunctionUseCase):
                self.blueprint.add_route(
                    http_view(route.use_case),
                    route.path,
                    methods=route.methods,
                )
            elif issubclass(route.use_case, use_cases.IteratorUseCase):
                self.blueprint.add_websocket_route(websocket_view(route.use_case), route.path)
        self.resources[resource.name] = resource
