import dataclasses
import typing


@dataclasses.dataclass(frozen=True)
class Request:
    params: typing.Mapping[str, typing.Any]
