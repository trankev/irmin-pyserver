import abc
import typing

import marshmallow

from irmin_pyserver import requests
from irmin_pyserver import response


ResourcesType = typing.TypeVar("ResourcesType")


class AbstractUseCase(abc.ABC, typing.Generic[ResourcesType]):
    @property
    @abc.abstractmethod
    def request_schema(self) -> marshmallow.Schema:
        raise NotImplementedError()

    ResponseDataSchema: typing.ClassVar[typing.Type[marshmallow.Schema]]
    ResponseMetadataSchema: typing.ClassVar[typing.Type[marshmallow.Schema]]

    def __init__(self, resources: ResourcesType) -> None:
        self.resources = resources


class FunctionUseCase(AbstractUseCase, abc.ABC):
    @abc.abstractmethod
    async def run(self, request: requests.Request) -> response.Response:
        raise NotImplementedError()


class IteratorUseCase(AbstractUseCase, abc.ABC):
    @abc.abstractmethod
    def run(self, request: requests.Request) -> typing.AsyncIterator[response.Response]:
        raise NotImplementedError()
