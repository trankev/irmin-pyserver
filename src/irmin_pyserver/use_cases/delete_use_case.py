import typing

import marshmallow
from marshmallow import fields

from irmin_pyserver import requests
from irmin_pyserver import response
from irmin_pyserver.datasources import base as datasources
from irmin_pyserver.use_cases import base


class RequestSchema(marshmallow.Schema):
    uid = fields.UUID(required=True)

    class Meta:
        strict = True


class DeleteUseCase(base.FunctionUseCase):
    datasource: typing.ClassVar[typing.Type[datasources.DocumentDeleteMixin]]
    request_schema = RequestSchema()

    async def run(self, request: requests.Request) -> response.Response:
        source = self.datasource(self.resources)
        await source.delete(request.params["uid"])
        return response.Response()
