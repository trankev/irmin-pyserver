import typing

import marshmallow
from marshmallow import fields

from irmin_pyserver import requests
from irmin_pyserver import response
from irmin_pyserver.datasources import base as datasources
from irmin_pyserver.use_cases import base


class RequestSchema(marshmallow.Schema):
    limit = fields.Int(missing=20)
    offset = fields.Int(missing=0)

    class Meta:
        strict = True


class ListUseCase(base.FunctionUseCase):
    resource_schema: marshmallow.Schema
    datasource: typing.ClassVar[typing.Type[datasources.DocumentIterateMixin]]
    request_schema = RequestSchema()

    async def run(self, request: requests.Request) -> response.Response:
        source = self.datasource(self.resources)
        count = await source.count({})
        query = source.iterate(
            filters={},
            limit=request.params["limit"],
            offset=request.params["offset"],
        )
        self.resource_schema.context = self.resources
        results = [self.resource_schema.dump(entry).data async for entry in query]
        return response.Response(data=results, metadata={"count": count})
