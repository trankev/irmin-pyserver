import typing

import marshmallow

from irmin_pyserver import requests
from irmin_pyserver import response
from irmin_pyserver.datasources import base as datasources
from irmin_pyserver.use_cases import base


class CreateUseCase(base.FunctionUseCase):
    datasource: typing.ClassVar[typing.Type[datasources.DocumentCreateMixin]]
    resource_schema: marshmallow.Schema
    fields: typing.ClassVar[typing.Optional[typing.Sequence[str]]] = None

    @property
    def request_schema(self) -> marshmallow.Schema:
        return self.resource_schema.__class__(only=self.fields)

    async def run(self, request: requests.Request) -> response.Response:
        self.resource_schema.context = {
            "request": request,
            "resources": self.resources,
        }
        entry = self.resource_schema.load(request.params).data
        source = self.datasource(self.resources)
        await source.create(entry)
        serialized = self.resource_schema.dump(entry).data
        return response.Response(data=serialized)
