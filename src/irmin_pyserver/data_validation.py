from __future__ import annotations

import os.path
import typing

import marshmallow
import sanic.request
import webargs
import webargs.core
from webargs_sanic import sanicparser

from irmin_pyserver import response


def parse_errors(
    errors: dict,
    *,
    base_path: str = "/",
) -> typing.Iterator[response.Error]:
    for key, value in errors.items():
        field = os.path.join(base_path, str(key))
        if isinstance(value, dict):
            yield from parse_errors(value, base_path=field)
        else:
            for message in value:
                yield response.Error(
                    code="input_validation_error",
                    title=message,
                    source=field,
                )


def handle_argument_errors(
        error: webargs.core.ValidationError,
        request: sanic.request.Request,
        schema: marshmallow.Schema,
) -> typing.NoReturn:
    print(type(error))
    raise response.ErrorResponse(errors=list(parse_errors(error.messages)))


def get_argparser() -> webargs.Parser:
    parser = sanicparser.SanicParser(error_handler=handle_argument_errors)
    return parser
