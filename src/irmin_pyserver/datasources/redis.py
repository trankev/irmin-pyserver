import abc
import typing
import uuid

import marshmallow
import ujson

from irmin_pyserver.datasources import base


class RedisDocument(base.DocumentCreateMixin, base.DocumentDeleteMixin, abc.ABC):
    key_pattern: typing.ClassVar[str]
    schema: marshmallow.Schema
    serializer = ujson

    async def create(self, entry: typing.Mapping[str, typing.Any]) -> None:
        key = self.key_pattern.format(uid=entry["uid"])
        expiration = self.resources.settings["SESSION"]["inactivity_expiration_timeout"]
        content = self.serializer.dumps(self.schema.dumps(entry).data)
        await self.resources.redis_client.set(key, content, ex=expiration)

    async def delete(self, entry_id: uuid.UUID) -> None:
        key = self.key_pattern.format(uid=entry_id)
        await self.resources.redis_client.delete(key)
