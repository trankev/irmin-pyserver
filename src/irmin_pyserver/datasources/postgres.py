from __future__ import annotations

import abc
import typing

import asyncpg
import marshmallow

from irmin_pyserver import errors
from irmin_pyserver import response as irmin_response
from irmin_pyserver.datasources import base


class PostgresDocument(base.DocumentCreateMixin, base.DocumentIterateMixin, abc.ABC):
    table_name: typing.ClassVar[str]
    schema: marshmallow.Schema

    async def iterate(
            self,
            filters: typing.Mapping[str, typing.Any],
            *,
            limit: int,
            offset: int,
    ) -> typing.AsyncIterable[typing.Mapping[str, typing.Any]]:
        async with self.resources.postgres_pool.acquire() as connection, connection.transaction():
            query = "SELECT {fields} FROM {table_name} LIMIT $1 OFFSET $2;"
            query = query.format(
                fields=",".join(field for field in self.schema.declared_fields.keys()),
                table_name=self.table_name,
            )
            cursor = connection.cursor(query, limit, offset)
            async for record in cursor:
                yield self.from_record(record)

    def from_record(self, record: asyncpg.Record) -> dict:
        return dict(record)

    async def count(self, filters: typing.Mapping[str, typing.Any]) -> int:
        query = "SELECT count(*) FROM {table_name};"
        query = query.format(table_name=self.table_name)
        async with self.resources.postgres_pool.acquire() as connection, connection.transaction():
            return await connection.fetchval(query)

    async def create(self, entry: typing.Mapping[str, typing.Any]) -> None:
        fields, values = zip(*entry.items())
        value_placeholders = ",".join(f"${index + 1}" for index in range(len(fields)))
        query = "INSERT INTO {table_name} ({fields}) VALUES ({value_placeholders});"
        query = query.format(
            table_name=self.table_name,
            fields=",".join(fields),
            value_placeholders=value_placeholders,
        )
        async with self.resources.postgres_pool.acquire() as connection, connection.transaction():
            try:
                await connection.execute(query, *values)
            except asyncpg.UniqueViolationError as exc:
                field = exc.constraint_name.split("_")[1]
                raise irmin_response.Conflict(errors=[
                    errors.duplicate_field(field=field, value=entry[field]),
                ])
