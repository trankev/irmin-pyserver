from __future__ import annotations

import abc
import typing
import uuid


class DataSource(abc.ABC):
    def __init__(self, resources: typing.Any) -> None:
        self.resources = resources


class DocumentIterateMixin(DataSource, abc.ABC):
    @abc.abstractmethod
    def iterate(
            self,
            filters: typing.Mapping[str, typing.Any],
            *,
            limit: int,
            offset: int,
    ) -> typing.AsyncIterable[typing.Mapping[str, typing.Any]]:
        raise NotImplementedError()

    @abc.abstractmethod
    async def count(self, filters: typing.Mapping[str, typing.Any]) -> int:
        raise NotImplementedError()


class DocumentCreateMixin(DataSource, abc.ABC):
    @abc.abstractmethod
    async def create(self, entry: typing.Mapping[str, typing.Any]) -> None:
        raise NotImplementedError()


class DocumentDeleteMixin(DataSource, abc.ABC):
    @abc.abstractmethod
    async def delete(self, entry_id: uuid.UUID) -> None:
        raise NotImplementedError()
