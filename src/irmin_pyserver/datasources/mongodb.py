import abc
import typing

from irmin_pyserver.datasources import base


class MongoDBDocument(base.DocumentIterateMixin, abc.ABC):
    collection_name: typing.ClassVar[str]

    async def iterate(
            self,
            filters: typing.Mapping[str, typing.Any],
            *,
            limit: int,
            offset: int,
    ) -> typing.AsyncIterable[typing.Mapping[str, typing.Any]]:
        collection = self.resources.mongo_database[self.collection_name]
        result = collection.find({}).limit(limit).skip(offset)
        async for entry in result:
            entry["uid"] = str(entry.pop("_id"))
            yield entry

    async def count(self, filters: typing.Mapping[str, typing.Any]) -> int:
        collection = self.resources.mongo_database[self.collection_name]
        return await collection.count_documents({})
