import contextlib
import socket
import time
import typing

import docker


ContainerIterator = typing.Iterator[docker.models.containers.Container]


def get_ip_address(container: docker.models.containers.Container) -> str:
    return container.attrs["NetworkSettings"]["IPAddress"]


@contextlib.contextmanager
def run_container(image: str, **run_params: typing.Any) -> ContainerIterator:
    client = docker.from_env()
    run_params.setdefault("detach", True)
    run_params.setdefault("auto_remove", True)
    container = client.containers.run(image, **run_params)
    while not get_ip_address(container):
        time.sleep(0.1)
        container = client.containers.get(container.name)
    try:
        yield container
    finally:
        container.remove(force=True)


def wait_for_port(
        ip_address: str,
        port: int,
        timeout: int = 5,
        delay: float = 0.1,
) -> None:
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    for _ in range(int(timeout / delay)):
        try:
            sock.connect((ip_address, int(port)))
            sock.shutdown(2)
        except ConnectionError:
            time.sleep(delay)
        else:
            break
