from irmin_pyserver import response as irmin_response


def duplicate_field(field: str, value: str) -> irmin_response.Error:
    return irmin_response.Error(
        code="duplicate_field",
        title=f"Another entry with same field value: {value}",
        source=f"/{field}",
    )
